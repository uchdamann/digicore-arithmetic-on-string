public class ArithmeticOnStringAlgorithm {

	public static void main(String[] args) {
		String X = "027";
		String Y = "-238";

		System.out.println("Sum is: " + addStr(X, Y));
	}

	public static String addStr(String num1, String num2) {
		StringBuilder sb = new StringBuilder("");
		int carryOver = 0;
		String sNum1 = num1;
		String sNum2 = num2;
		if (num1.charAt(0) == '-' && num2.charAt(0) == '-') {
			sNum1 = num1.replace("-", "");
			sNum2 = num2.replace("-", "");
		}
		if (num1.charAt(0) == '-' && num2.charAt(0) != '-') {
			String s;
			sNum1 = num1.replace("-", "");
			if (comparePosIntStr(sNum2, sNum1)) {
				s = minusStr(sNum2, sNum1);
				s = removePrefixZeroes(s);
				sb = new StringBuilder(s);
				return sb.reverse().append("-").reverse().toString();
			}
			else {
				s = minusStr(sNum1, sNum2);
				s = removePrefixZeroes(s);
				sb = new StringBuilder(s);
				return sb.reverse().append("-").reverse().toString();
			}
		}
		if (num1.charAt(0) != '-' && num2.charAt(0) == '-') {
			String s;
			sNum2 = num2.replace("-", "");
			if (comparePosIntStr(sNum2, sNum1)) {
				s = minusStr(sNum2, sNum1);
				s = removePrefixZeroes(s);
				sb = new StringBuilder(s);
				return sb.reverse().append("-").reverse().toString();
			}
			else {
				s = minusStr(sNum1, sNum2);
				return removePrefixZeroes(s);
			}
		}
		
		int i = sNum1.length() - 1;
		int j = sNum2.length() - 1;
		
		while (i >= 0 || j >= 0) {
			int sum = carryOver + (i < 0 ? 0 : sNum1.charAt(i) - 48);
			sum += j < 0 ? 0 : sNum2.charAt(j) - 48;
			sb.append(sum % 10);
			carryOver = sum / 10;
			--i;
			--j;
		}
		sb = sb.append(carryOver == 1 ? "1" : "");
		if (num1.charAt(0) == '-' && num2.charAt(0) == '-') {
			sb.append("-");
		}		
		return sb.reverse().toString();
	}
	
	public static String minusStr(String num1, String num2) {
		StringBuilder sb = new StringBuilder();
		boolean hasCarry = false;
		String sNum1 = num1;
		String sNum2 = num2;
		
		if (num1.charAt(0) == '-' && num2.charAt(0) == '-') {
			sNum1 = num1.replace("-", "");
			sNum2 = num2.replace("-", "");
		}

		if (num1.charAt(0) == '-' && num2.charAt(0) != '-') {
			sNum1 = num1.replace("-", "");
			sb = new StringBuilder(addStr(sNum1, sNum2));
			return sb.reverse().append("-").reverse().toString();
		}
		
		if (num1.charAt(0) != '-' && num2.charAt(0) == '-') {
			sNum2 = num2.replace("-", "");
			return addStr(sNum1, sNum2);
		}

		int i = sNum1.length() - 1;
		int j = sNum2.length() - 1;

		while (i >= 0 || j >= 0) {
			int diff = sNum1.charAt(i) - 48;
			if (hasCarry) {
				diff -= 1;
				if (diff < 0) {
					diff += 10;
					hasCarry = true;
				}
			}
			if (diff < (j < 0 ? 0 : sNum2.charAt(j) - 48)) {
				diff += 10;
				hasCarry = true;
			}
			diff -= (j < 0 ? 0 : (sNum2.charAt(j) - 48));
			sb.append(diff);
			--i;
			--j;
		}

		if (num1.charAt(0) == '-' && num2.charAt(0) == '-') {			
			sb.append("-");
		}
		return sb.toString();
	}
	
	public static boolean comparePosIntStr (String num1, String num2) {
		boolean isGreater = false;
		num1 = removePrefixZeroes(num1);
		num2 = removePrefixZeroes(num2);
		
		if (num1.length() > num2.length()) {
			isGreater = true;
		}
		else if (num1.length() == num2.length()) {
			for (int i = 0; i < num1.length(); i++) {
				if ((num1.charAt(0)) > (num2.charAt(0))) {
					isGreater = true;
					break;
				}
				else if ((num1.charAt(0)) < (num2.charAt(0))) {
					isGreater = false;
					break;
				}
			}
		}
		return isGreater;
	}
	
	public static String removePrefixZeroes(String num) {
		StringBuilder sb = new StringBuilder(num);
		while (sb.charAt(0)=='0') {
			sb.deleteCharAt(0);
		}
		return sb.toString();
	}
}